using System;

public class PercentagemOcupacaoException : Exception
{
    public PercentagemOcupacaoException() : base() { }
    public PercentagemOcupacaoException(string message) : base(message) { }
    public PercentagemOcupacaoException(string message, System.Exception inner) : base(message, inner) { }

    // A constructor is needed for serialization when an
    // exception propagates from a remoting server to the client. 
    protected PercentagemOcupacaoException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) { }
}
using System;

namespace SiCProject.Models
{
    public class CorProduto
    {
        public int ID {get; set;}

        public int ProdutoID {get; set;}

        public int CorID {get; set;}
    }
}
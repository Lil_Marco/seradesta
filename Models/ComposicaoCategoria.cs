using System;
using System.Collections.Generic;

namespace SiCProject.Models
{
    public class ComposicaoCategoria
    {
        public int ID {get; set;}

        public int CategoriaPaiID {get; set;}

        public int CategoriaFilhoID {get; set;}
    }
}
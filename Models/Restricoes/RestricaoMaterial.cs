using System;

namespace SiCProject.Models.Restricoes
{
    public class RestricaoMaterial : Restricao
    {
        public string material {get; set;}

        public override bool checkRestricao(String info){

            string[] materiais = info.Split(':');

            if(materiais[0].Equals(materiais[1])){
                return true;
            }
            return false;
        }
    }
}
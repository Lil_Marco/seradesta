using System;

namespace SiCProject.Models
{
    public class Dimensao
    {
        public int ID {get; set;}
        public int alturaID {get; set;}
        public Medida altura {get; set;}
        public int larguraID {get; set;}
        public Medida largura {get; set;}
        public int profundidadeID {get; set;}
        public Medida profundidade {get; set;}

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.DTOs;
using SiCProject.Models;
using SiCProject.Repositories;

namespace SiCProject
{
    [Route("api/[controller]")]
    [ApiController]
    public class ColecaoController : ControllerBase
    {
        private readonly SiCContext _context;
        private ColecaoRepository repo;

        public ColecaoController(SiCContext context)
        {
            _context = context;
            repo = new ColecaoRepository(context);
        }

        // GET: api/Colecaos
        [HttpGet]
        public IEnumerable<ColecaoDTO> GetColecao()
        {

            List<ColecaoDTO> lDTO = new List<ColecaoDTO>();
            foreach(Colecao c in repo.GetAll())
            {

                lDTO.Add(repo.getInfoColecaoDTO(c));
            }

            return lDTO;
        }

        // GET: api/Colecaos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetColecaoById([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Colecao colecao = repo.GetByID(id);

            if (colecao == null)
            {
                return NotFound();
            }

            return Ok(repo.getInfoColecaoDTO(colecao));
        }

        // PUT: api/Colecaos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutColecao([FromRoute] int id, [FromBody] Colecao colecao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != colecao.ID)
            {
                return BadRequest();
            }

            if(repo.Put(colecao)){
                 return NoContent();
            }else{
                return NotFound();
            }
        }

        // POST: api/Colecaos
        [HttpPost]
        public async Task<IActionResult> PostColecao([FromBody] Colecao colecao)
        {
           if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Colecao col = repo.Post(colecao);

            return Ok(repo.getInfoColecaoDTO(col));
        }

        // DELETE: api/Colecaos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteColecao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(repo.Delete(id)){
                return NoContent();
            }else{
                return NotFound();
            }
        }

        private bool ColecaoExists(int id)
        {
            return _context.Colecao.Any(e => e.ID == id);
        }
    }
}
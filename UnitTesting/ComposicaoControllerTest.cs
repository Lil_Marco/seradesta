using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Controllers;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class ComposicaoControllerTest
    {
        private SiCContext InitDBSet(String DbName){
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        #pragma warning disable 1998

        private void DB(SiCContext _context){

            Composicao c1 = new Composicao();

            Composicao c2 = new Composicao();

            _context.Composicoes.Add(c1);
            _context.Composicoes.Add(c2);

            _context.SaveChanges();
        }

        /*[Fact]
        public void Test_GetAll(){
            using(var _context = InitDBSet("Test_GetAll_C")){

                DB(_context);
                var _mController = new ComposicaoController(_context);
                long tamanho_esperado = 2;

                int resultado = 0;
                IEnumerable<Composicao> lol = _context.Composicoes;
                using(IEnumerator<Composicao> CharEnumerator = lol.GetEnumerator()){
                    while(CharEnumerator.MoveNext())
                        resultado++;
                }

                Assert.Equal(tamanho_esperado, resultado);
            }
        }*/

    [Fact]
        public async void Test_PutComposicao()
        {
            using (var _context = InitDBSet("Test_PutComposicao_M"))
            {
                var _mController = new ComposicaoController(_context);
                Assert.Equal(0, await _context.Composicoes.CountAsync());

                Composicao m = new Composicao();

                await _context.Composicoes.AddAsync(m);
                await _context.SaveChangesAsync();

                Assert.Equal(1, await _context.Composicoes.CountAsync());

                int mid = 1;

                var acab = await _mController.PutComposicao(mid, m);

            }
        }
        [Fact]
        public async void Test_PostComposicao()
        {
            using (var _context = InitDBSet("Test_PostComposicao"))
            {
                DB(_context);
                var _mController = new ComposicaoController(_context);

                Composicao m = new Composicao();
            }
        }

        [Fact]
        public async void Test_DeleteComposicao()
        {
            using (var _context = InitDBSet("Test_DeleteComposicao_M"))
            {
                Composicao m = new Composicao();

                _context.Composicoes.Add(m);

                _context.SaveChanges();

                var _mController = new ComposicaoController(_context);

                var mat = await _mController.DeleteComposicao(m.ID);
                Assert.IsType<OkObjectResult>(mat);
            }
        }

        [Fact]
        public async void Test_GetComposicao()
        {
            using (var _context = InitDBSet("Test_GetComposicao"))
            {
                DB(_context);
                var _mController = new ComposicaoController(_context);

                Composicao m = await _context.Composicoes.FirstAsync();

                var c = await _mController.GetComposicao(m.ID);
                Assert.IsType<OkObjectResult>(c);
            }
        }
    }
}
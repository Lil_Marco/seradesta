/*using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Controllers;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class CategoriaControllerTest
    {
        private SiCContext InitDBSet(String DbName){
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context){

            Categoria c1 = new Categoria{
                nome = "armario"
            };

            Categoria c2 = new Categoria{
                nome = "gaveta"
            };

            _context.Categorias.Add(c1);
            _context.Categorias.Add(c2);

            _context.SaveChanges();
        }

        [Fact]
        public void Test_GetAll(){
            using(var _context = InitDBSet("Test_GetAll_C")){

                DB(_context);
                var _mController = new CategoriaController(_context);
                long tamanho_esperado = 2;

                int resultado = 0;
                IEnumerable<Categoria> lol = _context.Categorias;
                using(IEnumerator<Categoria> CharEnumerator = lol.GetEnumerator()){
                    while(CharEnumerator.MoveNext())
                        resultado++;
                }

                Assert.Equal(tamanho_esperado, resultado);
            }
        }

    [Fact]
        public async void Test_PutCategoria()
        {
            using (var _context = InitDBSet("Test_PutCategoria_M"))
            {
                var _mController = new CategoriaController(_context);
                Assert.Equal(0, await _context.Categorias.CountAsync());

                Categoria m = new Categoria
                {
                    nome = "armario_test"
                };

                await _context.Categorias.AddAsync(m);
                await _context.SaveChangesAsync();

                Assert.Equal(1, await _context.Categorias.CountAsync());

                int mid = 1;
                m.nome = "Test";

                var acab = await _mController.PutCategoria(mid, m);

            }
        }
        [Fact]
        public async void Test_PostCategoria()
        {
            using (var _context = InitDBSet("Test_PostCategoria"))
            {
                DB(_context);
                var _mController = new CategoriaController(_context);

                Categoria m = new Categoria();

                var cat = await _mController.PostCategoria(m);
                Assert.IsType<CreatedAtActionResult>(cat);

            }
        }

        [Fact]
        public async void Test_DeleteCategoria()
        {
            using (var _context = InitDBSet("Test_DeleteCategoria_M"))
            {
                Categoria m = new Categoria
                {
                    nome = "armario"
                };

                _context.Categorias.Add(m);

                _context.SaveChanges();

                var _mController = new CategoriaController(_context);

                var cat = await _mController.DeleteCategoria(m.ID);
                Assert.IsType<OkObjectResult>(cat);
            }
        }

        [Fact]
        public async void Test_GetCategoriaByID()
        {
            using (var _context = InitDBSet("Test_GetCategoriaByID"))
            {
                DB(_context);
                var _mController = new CategoriaController(_context);

                Categoria m = await _context.Categorias.FirstAsync();

                var cat = await _mController.GetCategoriaByID(m.ID);
                Assert.IsType<OkObjectResult>(cat);
            }
        }
    }
}*/
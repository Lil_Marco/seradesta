using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Controllers;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class MedidaControllerTest
    {
        private SiCContext InitDBSet(String DbName){
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context){

            Medida m1 = new Medida{
                valor = 2
            };

            Medida m2 = new Medida{
                valor = 3
            };

            _context.Medidas.Add(m1);
            _context.Medidas.Add(m2);

            _context.SaveChanges();
        }

        //[Fact]
        /*public void Test_GetAll(){
            using(var _context = InitDBSet("Test_GetAll_C")){

                DB(_context);
                var _mController = new MedidaController(_context);
                long tamanho_esperado = 2;

                int resultado = 0;
                IEnumerable<Medida> lol = _context.Medidas;
                using(IEnumerator<Medida> CharEnumerator = lol.GetEnumerator()){
                    while(CharEnumerator.MoveNext())
                        resultado++;
                }

                Assert.Equal(tamanho_esperado, resultado);
            }
        }*/

    [Fact]
        public async void Test_PutMedida()
        {
            using (var _context = InitDBSet("Test_PutMedida_M"))
            {
                var _mController = new MedidaController(_context);
                Assert.Equal(0, await _context.Medidas.CountAsync());

                Medida m = new Medida
                {
                    valor = 2
                };

                await _context.Medidas.AddAsync(m);
                await _context.SaveChangesAsync();

                Assert.Equal(1, await _context.Medidas.CountAsync());

                int mid = 1;
                m.valor = 3;

                var acab = await _mController.PutMedida(mid, m);

            }
        }
        [Fact]
        public async void Test_PostMedida()
        {
            using (var _context = InitDBSet("Test_PostMedida"))
            {
                DB(_context);
                var _mController = new MedidaController(_context);

                Medida m = new Medida();

                var mat = await _mController.PostMedida(m);
                Assert.IsType<CreatedAtActionResult>(mat);

            }
        }

        [Fact]
        public async void Test_DeleteMedida()
        {
            using (var _context = InitDBSet("Test_DeleteMedida_M"))
            {
                Medida m = new Medida
                {
                    valor = 2
                };

                _context.Medidas.Add(m);

                _context.SaveChanges();

                var _mController = new MedidaController(_context);

                var med = await _mController.DeleteMedida(m.ID);
                Assert.IsType<OkObjectResult>(med);
            }
        }

        [Fact]
        public async void Test_GetMedida()
        {
            using (var _context = InitDBSet("Test_GetMedida"))
            {
                DB(_context);
                var _mController = new MedidaController(_context);

                Medida m = await _context.Medidas.FirstAsync();

                var med = await _mController.GetMedida(m.ID);
                Assert.IsType<OkObjectResult>(med);
            }
        }
    }
}
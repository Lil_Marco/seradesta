using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Controllers;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class DimensaoControllerTest
    {
        private SiCContext InitDBSet(String DbName){
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context){

            Dimensao d1 = new Dimensao{
                altura = null,
                largura = null,
                profundidade =null
            };

            Dimensao d2 = new Dimensao{
                altura = null,
                largura = null,
                profundidade = null
            };

            _context.Dimensoes.Add(d1);
            _context.Dimensoes.Add(d2);

            _context.SaveChanges();
        }

        [Fact]
        public void Test_GetAll(){
            using(var _context = InitDBSet("Test_GetAll_C")){

                DB(_context);
                var _mController = new DimensaoController(_context);
                long tamanho_esperado = 2;

                int resultado = 0;
                IEnumerable<Dimensao> lol = _context.Dimensoes;
                using(IEnumerator<Dimensao> CharEnumerator = lol.GetEnumerator()){
                    while(CharEnumerator.MoveNext())
                        resultado++;
                }

                Assert.Equal(tamanho_esperado, resultado);
            }
        }

    [Fact]
        public async void Test_PutDimensao()
        {
            using (var _context = InitDBSet("Test_PutDimensao_M"))
            {
                var _mController = new DimensaoController(_context);
                Assert.Equal(0, await _context.Dimensoes.CountAsync());

                Dimensao m = new Dimensao
                {
                    altura=null,
                    largura=null,
                    profundidade=null
                };

                await _context.Dimensoes.AddAsync(m);
                await _context.SaveChangesAsync();

                Assert.Equal(1, await _context.Dimensoes.CountAsync());

                int mid = 1;

                var acab = await _mController.PutDimensao(mid, m);

            }
        }
        [Fact]
        public async void Test_PostDimensao()
        {
            using (var _context = InitDBSet("Test_PostDimensao"))
            {
                DB(_context);
                var _mController = new DimensaoController(_context);

                Dimensao m = new Dimensao();

                var dim = await _mController.PostDimensao(m);
                Assert.IsType<CreatedAtActionResult>(dim);

            }
        }

        [Fact]
        public async void Test_DeleteDimensao()
        {
            using (var _context = InitDBSet("Test_DeleteDimensao_M"))
            {
                Dimensao m = new Dimensao
                {
                    altura=null,
                    largura=null,
                    profundidade=null
                };

                _context.Dimensoes.Add(m);

                _context.SaveChanges();

                var _mController = new DimensaoController(_context);

                var dim = await _mController.DeleteDimensao(m.ID);
                Assert.IsType<OkObjectResult>(dim);
            }
        }

        [Fact]
        public async void Test_GetDimensao()
        {
            using (var _context = InitDBSet("Test_GetDimensao"))
            {
                DB(_context);
                var _mController = new DimensaoController(_context);

                Dimensao m = await _context.Dimensoes.FirstAsync();

                var dim = await _mController.GetDimensao(m.ID);
                Assert.IsType<OkObjectResult>(dim);
            }
        }
    }
}